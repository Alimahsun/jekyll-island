---
layout: page
title: contact
permalink: /land/contact/
---

To link up with me, [send me an email](mailto:mwb71@case.edu).

If PGP is important to you, you can [download my key](/assets/pgp/pgp.asc) (or get it from a keyserver) and [verify the stuff I'm saying](/assets/pgp/contact.asc).
